<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'VidaCar', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'VidaCar' ) );

//* Add Image upload and Color select to WordPress Theme Customizer
require_once( get_stylesheet_directory() . '/lib/customize.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'VidaCar' );
define( 'CHILD_THEME_URL', 'http://designify.com/' );
define( 'CHILD_THEME_VERSION', '1.0.' );

//* Enqueue Styles and Scripts
add_action( 'wp_enqueue_scripts', 'genesis_sample_scripts' );
function genesis_sample_scripts() {

	//* Add Google Fonts
	wp_register_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Oswald:300,400,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'google-fonts' );

	//* Add JS
	wp_enqueue_script( 'genesis-sample-scripts', get_stylesheet_directory_uri() . '/js/project-min.js', array( 'jquery' ), '1.0.0', true );
/*
	wp_enqueue_script ( 'matchheight' , get_stylesheet_directory_uri() . '/js/partials/jquery.matchHeight-min.js', array( 'jquery' ), '1', true );
	 wp_enqueue_script ( 'matchheight-init' , get_stylesheet_directory_uri() . '/js//partials/matchheight-init.js', array( 'matchheight' ), '1', true );
*/

	//* Add Dashicons
	 wp_enqueue_style( 'dashicons' );
	
}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
// add_theme_support( 'custom-background' );

//* Add support for 2-column footer widgets
add_theme_support( 'genesis-footer-widgets', 2 );

//* Removing emoji code form head
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

//* Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

//* Remove the header right widget area
unregister_sidebar( 'header-right' );

//* Remove the secondary sidebar
unregister_sidebar( 'sidebar-alt' );

//* Rename menus
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Left Navigation Menu', 'VidaCar' ), 'secondary' => __( 'Right Navigation Menu', 'VidaCar' ) ) );

//* Hook menus
add_action( 'genesis_after_header', 'vidacar_menus_container' );
function vidacar_menus_container() {

	echo '<div class="navigation-container">';
	do_action( 'vidacar_menus' );
	echo '</div>';
	
}

//* Relocate Primary (Left) Navigation
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'vidacar_menus', 'genesis_do_nav' );

//* Relocate Secondary (Right) Navigation
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'vidacar_menus', 'genesis_do_subnav' );

//* Remove output of primary navigation right extras
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

//* Customize the credits
add_filter( 'genesis_footer_creds_text', 'sp_footer_creds_text' );
function sp_footer_creds_text() {
	echo '<div class="creds"><p>';
	echo 'Copyright &copy; ';
	echo date('Y');
	echo ' &middot; VidaCar';
	echo '</p></div>';
}

//* Change Post label to News
function designnify_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
function designnify_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'designnify_change_post_label' );
add_action( 'init', 'designnify_change_post_object' );

// Remove Post Info, Post Meta from CPT
function vidacar_remove_post_info() {
	if ('cpt_service' == get_post_type()) {//add in your CPT name
		remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		}
}
add_action ( 'get_header', 'vidacar_remove_post_info' );

//* Display Featured Image in Single Post Page (Not Blog Page)
add_action( 'genesis_entry_content', 'featured_post_image', 8 );
function featured_post_image() {
  if ( ! is_singular( 'post' ) )  return;
	the_post_thumbnail('post-image');
}

//* Remove the entry meta in the entry header
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

//* Remove the entry meta in the entry footer
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );

//* Register widget areas for home page
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home Top', 'VidaCar' ),
	'description' => __( 'This is the home top section of the Home Page.', 'VidaCar' ),
) );
genesis_register_sidebar( array(
	'id'          => 'bellow-home-top',
	'name'        => __( 'Bellow Home Top ', 'VidaCar' ),
	'description' => __( 'This is the section bellow home top of the Home Page.', 'VidaCar' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-middle',
	'name'        => __( 'Home Middle', 'VidaCar' ),
	'description' => __( 'This is the home middle section of the Home Page.', 'VidaCar' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home Bottom', 'VidaCar' ),
	'description' => __( 'This is the home bottom section of the Home Page.', 'VidaCar' ),
) );